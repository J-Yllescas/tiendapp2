//
//  TarjetaViewController.swift
//  iOSApp
//
//  Created by MAC on 07/01/20.
//  Copyright © 2020 Javier Yllescas. All rights reserved.
//

import UIKit
import CoreData

class TarjetaViewController: UIViewController {

    @IBOutlet var tfNombre: UITextField!
    @IBOutlet var tfTarjeta: UITextField!
    @IBOutlet var tfDia: UITextField!
    @IBOutlet var tfMes: UITextField!
    @IBOutlet var tfCvv: UITextField!
    
    @IBOutlet var Viewnombre: UIView!
    @IBOutlet var Viewtarjeta: UIView!
    @IBOutlet var Viewdia: UIView!
    @IBOutlet var Viewcvv: UIView!
    @IBOutlet var Viewmes: UIView!
    
    var direccioin:[String:String]?
    var Lista:[Dictionary<String, Any>] = []
    var total:String?
    
    var user:NSManagedObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in
                result as! [NSManagedObject]{
                    user = data
            }
        }catch {
            print("Failed")
        }
    }
    
    @IBAction func Continuar(_ sender: Any) {
        guard let noombre = tfNombre.text else {return}
        guard let tarjeta = tfTarjeta.text else {return}
        guard let dia = tfDia.text else {return}
        guard let mes = tfMes.text else {return}
        guard let cvv = tfCvv.text else {return}
        
        if noombre.espacionBlanco(cadena: noombre) == "" {
            alert(mensaje: "por favor llene el campo Nombre")
            Viewnombre.backgroundColor = UIColor.red
        }
        else{
            if noombre.alfanumerico(cadena: noombre.lowercased()) == false {
                alert(mensaje: "Revise que el campo 'Nombre' sólo contenga caracteres alfanuméricos")
                Viewnombre.backgroundColor = UIColor.red
            }
            else {
                Viewnombre.backgroundColor = UIColor.black
                if tarjeta.espacionBlanco(cadena: tarjeta) == "" {
                    alert(mensaje: "por favor llene el campo tajeta")
                    Viewtarjeta.backgroundColor = UIColor.red
                }
                else {
                    if tarjeta.numerico(cadena: tarjeta.lowercased()) == false {
                        alert(mensaje: "Revise que el campo tarjeta solo contenga caracteres numericos")
                        Viewtarjeta.backgroundColor = UIColor.red
                    }
                    else{
                        if tarjeta.count < 16 {
                            alert(mensaje: "no puede contener menos de 16 digitos")
                            Viewtarjeta.backgroundColor = UIColor.red
                        }
                        else {
                            Viewtarjeta.backgroundColor = UIColor.black
                            if dia.espacionBlanco(cadena: dia) == "" {
                                alert(mensaje: "por favor llene el campo mes")
                                Viewdia.backgroundColor = UIColor.red
                            }
                            else {
                               if dia.numerico(cadena: dia.lowercased()) == false {
                                alert(mensaje: "Revise que el campo mes solo contenga carcteres numericos")
                                Viewdia.backgroundColor = UIColor.red
                               }
                               else{
                                if dia.count < 2 {
                                    alert(mensaje: "no puede contener menos de 2 digitos")
                                    Viewdia.backgroundColor = UIColor.red
                                }else{
                                    Viewdia.backgroundColor = UIColor.black
                                    if mes.espacionBlanco(cadena: mes) == "" {
                                        alert(mensaje: "por favor llene el campo año")
                                        Viewmes.backgroundColor = UIColor.red
                                    }
                                    else{
                                        if mes.numerico(cadena: mes.lowercased()) == false{
                                            alert(mensaje: "Revise que el campo año solo contenga carcteres numericos")
                                            Viewmes.backgroundColor = UIColor.red
                                        }
                                        else{
                                            if mes.count < 4 {
                                                alert(mensaje: "no puede contener menos de 4 digitos")
                                                Viewmes.backgroundColor = UIColor.red
                                            }
                                            else{
                                                Viewmes.backgroundColor = UIColor.black
                                                if cvv.espacionBlanco(cadena: cvv) == "" {
                                                    alert(mensaje: "por favor llene el campo CVV")
                                                    Viewcvv.backgroundColor = UIColor.red
                                                }
                                                else{
                                                    if cvv.numerico(cadena: cvv.lowercased()) == false{
                                                        alert(mensaje: "Revise que el campo dia solo contenga carcteres numericos")
                                                        Viewcvv.backgroundColor = UIColor.red
                                                    }
                                                    else{
                                                        if cvv.count < 3 {
                                                            alert(mensaje: "no puede contener menos de 3 digitos")
                                                            Viewcvv.backgroundColor = UIColor.red
                                                        }else{
                                                            Viewcvv.backgroundColor = UIColor.black
                                                            let vencimiento:String = "\(dia)/\(mes)"
                                                            let datostarjeta:[String:String] = ["nombre":noombre, "numero":tarjeta]
                                                            guardarTarjeta(idCliente: user?.value(forKey: "idCliente") as! String, token: user?.value(forKey: "token") as! String, nombre: noombre, numero: tarjeta, vencimiento: vencimiento, cvv: cvv, callback: {
                                                                json in
                                                                if json["codigoOperacion"] as? Int == 0 {
                                                                    DispatchQueue.main.async {
                                                                        let alert = UIAlertController(title: "TiendApp", message: "Tarjeta registrada", preferredStyle: .alert)
                                                                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                                                            let story = UIStoryboard(name: "Main", bundle: nil)
                                                                            let control = story.instantiateViewController(identifier: "Resumen") as! ResumenViewController
                                                                            control.Lista = self.Lista
                                                                            control.total = self.total
                                                                            control.direccioin = self.direccioin
                                                                            control.tarjeta = datostarjeta
                                                                            self.present(control, animated: true, completion: nil)
                                                                        }))
                                                                        self.present(alert, animated: true, completion: nil)
                                                                    }
                                                                }
                                                                else {
                                                                    self.alert(mensaje: "Error al agregar tarjeta")
                                                                }
                                                            })
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                             }
                          }
                       }
                    }
                }
            }
        }
    }
   
    func alert(mensaje:String){
        let alert = UIAlertController(title: "TiendApp", message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func guardarTarjeta(idCliente:String, token:String, nombre:String, numero:String, vencimiento:String, cvv:String, callback: @escaping ([String:Any]) -> ()) {
        
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/registrartarjeta") else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let param:[String:Any] = ["idCliente":idCliente, "token":token, "nombre":nombre, "numero":numero, "vencimiento":vencimiento, "cvv":cvv]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            print("fail")
            return
        }
        print(String(data: jsonData, encoding: String.Encoding.utf8)!)
        
        request.httpBody = jsonData
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print(error!)
            }
            else{
                do {
                    guard let content = data else {
                        print("no data")
                        return
                    }
                    
                    guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                        print("Bad json")
                        return
                    }
                    print(jsonArray)
                    callback(jsonArray)
                }
            }
        }.resume()
    }
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}
       
                    
  
    
 

