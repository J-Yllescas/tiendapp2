//
//  FinalViewController.swift
//  iOSApp
//
//  Created by Javier Yllescas on 1/10/20.
//  Copyright © 2020 Javier Yllescas. All rights reserved.
//

import UIKit

class FinalViewController: UIViewController {

    @IBOutlet weak var lbFolio: UILabel!
    
    var folio:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let f = folio else {return}
        lbFolio.text = "Folio: \(f)"
    }
}
