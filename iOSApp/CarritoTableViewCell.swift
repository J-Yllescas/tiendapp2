//
//  CarritoTableViewCell.swift
//  iOSApp
//
//  Created by Javier Yllescas on 1/3/20.
//  Copyright © 2020 Javier Yllescas. All rights reserved.
//

import UIKit
import CoreData

protocol Eliminar {
    func eliminar(index:Int)
    func editar(index:Int, valor: Int)
}

class CarritoTableViewCell: UITableViewCell {

    @IBOutlet weak var img01: UIImageView!
    @IBOutlet weak var lbNombre: UILabel!
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var lbPrecio: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var lbStep: UILabel!
    
    var index:Int?
    var delegate:Eliminar?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func stepperAction(_ sender: UIStepper) {
        lbStep.text = Int(sender.value).description
        guard let aux = index else {
            return
        }
        guard let stp = lbStep.text else {return}
        delegate?.editar(index: aux, valor: Int(stp)!)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func Eliminar(_ sender: Any) {
        guard let aux = index else {
            return
        }
        delegate?.eliminar(index: aux)
    }
    
}
