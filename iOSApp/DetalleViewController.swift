//
//  DetalleViewController.swift
//  iOSApp
//
//  Created by Javier Yllescas on 12/31/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit
import CoreData

class DetalleViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var img01: UIImageView!
    @IBOutlet weak var lbDescripcion: UILabel!
    @IBOutlet weak var lbPrecio: UILabel!
    @IBOutlet weak var lbDisponible: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var btnAgregar: UIButton!
    @IBOutlet weak var lbStep: UILabel!
    
    var product:Product?
    var productNS:[NSManagedObject] = []
    var bandera = true
    
    //guards
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBar.topItem?.title = product?.nombre
        stepper.wraps = false
        stepper.autorepeat = true
        stepper.maximumValue = Double((product?.stock!)!)
        
        print(product!)
        let newURL = product?.imagensrc!.replacingOccurrences(of: "\"", with: "")
        print(newURL!)
        let url = URL(string: "\(newURL!).jpg")
        print(url!)
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            if error == nil {
                DispatchQueue.main.async {
                    let image = UIImage(data: data!)
                    self.img01.image = image
                }
            }
        }
        task.resume()
        
        lbDescripcion.text = product?.descripcion!
        lbPrecio.text = "Precio: $\(String(describing: product!.precio!))"
        lbDisponible.text = "Disponible: \(String(describing: product!.stock!))"
        
        if product?.stock! == 0 {
            btnAgregar.isEnabled = false
            btnAgregar.backgroundColor = UIColor.red
            lbDisponible.textColor = UIColor.red
            lbStep.text = "0"
        }
    }
    
    @IBAction func stepperAction(_ sender: UIStepper) {
        lbStep.text = Int(sender.value).description
    }
    
    @IBAction func Agregar(_ sender: Any) {
        enviar(idProducto: (product?.id)!, stock: lbStep.text!, callback: { json in
            if json["codigoOperacion"] as! Int == 0 {
                self.bandera = true
            }
            else{
                self.bandera = false
            }
        })
        
        if bandera == true{
            var existe:Bool = false
            print(productNS.count)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Producto")
            request.returnsObjectsAsFaults = false
            
            do {
                let result = try context.fetch(request)
                for data in
                    result as! [NSManagedObject]{
                        self.productNS.append(data)
                }
            }catch {
                print("Failed")
            }
            
                for producto in self.productNS {
                    if producto.value(forKey: "idProducto") as? String == self.product?.id {
                        let aux:Int16 = (producto.value(forKey: "cantidad") as? Int16)!
                        let aux2:Int16 = Int16(lbStep.text!)!
                        if aux + aux2 > producto.value(forKey: "stock") as! Int16 {
                            alert(mensaje: "No se puede agregar mas productos")
                            existe = true
                        }
                        else {
                            producto.setValue(aux + aux2, forKey: "cantidad")
                            let aux3:Double = producto.value(forKey: "total") as! Double
                            let aux4:Double = (self.product?.precio)! * Double(lbStep.text!)!
                            producto.setValue(aux3 + aux4, forKey: "total")
                            existe = true
                            do{
                                try context.save()
                            }catch let error as NSError{
                                print(error.userInfo)
                            }
                            alert(mensaje: "Producto agregado con exito")
                        }
                    }
                }
            
            if existe == false {
                let entity = NSEntityDescription.entity(forEntityName: "Producto", in:
                context)
                let newProduct = NSManagedObject(entity: entity!, insertInto: context)
                newProduct.setValue(Int16(lbStep.text!)!, forKey: "cantidad")
                newProduct.setValue((self.product?.nombre)!, forKey: "nombre")
                newProduct.setValue(self.product?.precio!, forKey: "precio")
                newProduct.setValue(self.product?.id!, forKey: "idProducto")
                let total1:Double = (self.product?.precio!)! * Double(lbStep.text!)!
                newProduct.setValue(total1, forKey: "total")
                newProduct.setValue(self.product?.imagensrc!, forKey: "img")
                newProduct.setValue(self.product?.stock!, forKey: "stock")
                alert(mensaje: "Producto agregado con exito")
                
                do{
                    try context.save()
                }catch let error as NSError{
                    print(error.userInfo)
                }
            }
        }
        else {
            alert(mensaje: "Error al agregar producto")
        }
        
    }
    
    func enviar(idProducto:String, stock:String, callback: @escaping ([String:Any]) -> ()){
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/verdisponibilidad") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let param:[String:String] = ["idProducto" : idProducto, "cantidad" : stock]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            print("fail")
            return
        }
                       
        print(String(data: jsonData, encoding: String.Encoding.utf8)!)
           
        request.httpBody = jsonData
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print(error!)
            }
            else{
                do {
                    guard let content = data else {
                        print("no data")
                        return
                    }
                       
                    guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                        print("Bad json")
                        return
                    }
                    
                    print(jsonArray)
                    callback(jsonArray)
                }
            }
        }.resume()
    }
    
    func alert(mensaje:String){
        let alert = UIAlertController(title: "TiendApp", message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
        
    }
    
}
