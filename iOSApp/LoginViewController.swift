//
//  LoginViewController.swift
//  iOSApp
//
//  Created by Javier Yllescas on 12/24/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit
import LocalAuthentication
import CoreData
import Security

class LoginViewController: UIViewController {

    let context = LAContext()
    var error:NSError?
    var strAlertMessage = String()
    var user:NSManagedObject?
    var recordar:NSManagedObject?
    var correo:String?
    var key:NSManagedObject?
    var publicKey:String?
    var privateKey:String?
    
    @IBOutlet weak var tfUser: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnFaceBiometric: UIButton!
    @IBOutlet weak var viewCorreo: UIView!
    @IBOutlet weak var viewContraseña: UIView!
    
    let rsa = RSA()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context2 = appDelegate.persistentContainer.viewContext
        
        let request3 = NSFetchRequest<NSFetchRequestResult>(entityName: "Llave")
        request3.returnsObjectsAsFaults = false
        
        do {
            let result = try context2.fetch(request3)
            for data in
                result as! [NSManagedObject]{
                    key = data
            }
        }catch {
            print("Failed")
        }
        
        if key == nil{
            rsa.generarLlave()
            print("no hay llave")
        }
        else {
            privateKey = (key?.value(forKey: "privateKey") as! String)
            publicKey = (key?.value(forKey: "publicKey") as! String)
            //print(publicKey!)
        }
        
        //ObtenerLlave()
        
        btnFaceBiometric.isEnabled = false
        
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context2.fetch(request)
            for data in
                result as! [NSManagedObject]{
                    user = data
                    print("Si hay usuario")
                    DispatchQueue.main.async {
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let control = story.instantiateViewController(identifier: "Lista") as! ListaViewController
                        self.present(control, animated: true, completion: nil)
                    }
            }
        }catch {
            print("Failed")
        }
        
        let request2 = NSFetchRequest<NSFetchRequestResult>(entityName: "Recordar")
        request2.returnsObjectsAsFaults = false
        
        do {
            let result = try context2.fetch(request2)
            for data in
                result as! [NSManagedObject]{
                    recordar = data
                    btnFaceBiometric.isEnabled = true
                    if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error){
                        switch context.biometryType {
                        case .faceID:
                            btnFaceBiometric.setImage(UIImage(named: "faceid"), for: UIControl.State.normal)
                            self.strAlertMessage = "Desea identificarse con FaceID?"
                            break
                        case .touchID:
                            btnFaceBiometric.setImage(UIImage(named: "touchid"), for: UIControl.State.normal)
                            self.strAlertMessage = "Desea identificarse con TouchID?"
                            break
                        case .none:
                            print("No biometric sensor")
                            break
                        default:
                            print("Other")
                        }
                    }
                    else{
                        if let err = error {
                            let strMessager = self.errorMessage(errorCode: err._code)
                            self.notifyUser("Error", err: strMessager)
                        }
                    }
            }
        }catch {
            print("Failed")
        }
        
        if correo != nil {
            tfUser.text = correo!
        }
        else {
            tfUser.text = recordar?.value(forKey: "user") as? String
        }
    }
    
    func notifyUser(_ msg:String, err:String){
        let alert = UIAlertController(title: msg, message: err, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
    
    func errorMessage(errorCode:Int) -> String {
        var strMessage = ""
        
        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            strMessage = "Authentication failed"
            break
        case LAError.userCancel.rawValue:
            strMessage = "User cancel"
            break
        case LAError.userFallback.rawValue:
            strMessage = "User Fallback"
            break
        case LAError.systemCancel.rawValue:
            strMessage = "System Cancel"
            break
        case LAError.passcodeNotSet.rawValue:
            strMessage = "Passcode not set"
            break
        case LAError.biometryNotAvailable.rawValue:
            strMessage = "Biometric not available"
            break
        case LAError.appCancel.rawValue:
            strMessage = "App cancel"
            break
        case LAError.biometryLockout.rawValue:
            strMessage = "Biometric locked"
            break
        default:
            strMessage = "Some Error Found"
        }
        
        return strMessage
    }
    
    @IBAction func identificar(_ sender: Any) {
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                                   localizedReason: self.strAlertMessage,
                                   reply: {[unowned self] (success, error) -> Void in
                                    DispatchQueue.main.async {
                                        if (success) {
                                            print("Identificado correctamente")
                                            self.enviar(correo: self.recordar?.value(forKey: "user") as! String, contrasena: self.recordar?.value(forKey: "password") as! String)
                                        }
                                        else {
                                            print("No identificado")
                                            if let error = error {
                                                let strMessage = self.errorMessage(errorCode: error._code)
                                                self.notifyUser("Error", err: strMessage)
                                            }
                                        }
                                    }
                                })
        }
    }
    

    @IBAction func Entrar(_ sender: Any) {
        guard let correo = tfUser.text else {return}
        guard let contrasena = tfPassword.text else {return}
        
        if correo.espacionBlanco(cadena: correo) == "" {
            alert(mensaje: "Por favor llene el campo 'Correo'")
            viewCorreo.backgroundColor = UIColor.red
        }
        else {
            if correo.correo(cadena: correo) == false {
                alert(mensaje: "El correo electrónico no es válido")
                viewCorreo.backgroundColor = UIColor.red
            }
            else {
                viewCorreo.backgroundColor = UIColor.black
                if contrasena.count < 8 {
                    alert(mensaje: "El campo 'Contraseña' debe tener como mínimo 8 caracteres")
                    viewContraseña.backgroundColor = UIColor.red
                }
                else {
                    viewContraseña.backgroundColor = UIColor.black
                    if (recordar?.value(forKey: "user") as? String == correo) && (recordar?.value(forKey: "password") as? String == contrasena) {
                        self.enviar(correo: correo, contrasena: contrasena)
                    }
                    else {
                        let alert = UIAlertController(title: "TiendApp", message: "¿Deseas guardar este usuario y contraseña para iniciar sesión con touchID y/o faceID?", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {[unowned self] action in
                            
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            let context = appDelegate.persistentContainer.viewContext
                            let entity = NSEntityDescription.entity(forEntityName: "Recordar", in:
                            context)
                            
                            if self.recordar == nil{
                                let newProduct = NSManagedObject(entity: entity!, insertInto: context)
                                newProduct.setValue(correo, forKey: "user")
                                newProduct.setValue(contrasena, forKey: "password")
                                
                                do {
                                  try context.save()
                                } catch let error as NSError {
                                  print("No ha sido posible guardar \(error), \(error.userInfo)")
                                }
                                
                                self.actualizarRecordar()
                            }
                            else {
                                self.recordar?.setValue(correo, forKey: "user")
                                self.recordar?.setValue(contrasena, forKey: "password")
                                
                                do {
                                  try context.save()
                                } catch let error as NSError {
                                  print("No ha sido posible guardar \(error), \(error.userInfo)")
                                }
                            }
                            print("\(String(describing: self.recordar?.value(forKey: "user")))")
                            self.enviar(correo: correo, contrasena: contrasena)
                        }))
                        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: {[unowned self] action in
                            print("\(String(describing: self.recordar?.value(forKey: "user")))")
                            self.enviar(correo: correo, contrasena: contrasena)
                        }))
                        present(alert, animated: true)
                    }
                }
            }
        }
    }
    
    func actualizarRecordar(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context2 = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recordar")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context2.fetch(request)
            for data in
                result as! [NSManagedObject]{
                    recordar = data
            }
        }catch {
            print("Failed")
        }
    }
    
    func printt() {
        print("chido")
    }
    
    func enviar(correo:String, contrasena:String) {
           
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/login") else { return }
           var request = URLRequest(url: url)
           request.httpMethod = "POST"
           request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
           let param:[String:String] = ["correo" : correo, "contrasena" : contrasena]
           guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
               print("fail")
               return
           }
                       
           print(String(data: jsonData, encoding: String.Encoding.utf8)!)
           
           request.httpBody = jsonData
           let session = URLSession.shared
           session.dataTask(with: request) { (data, response, error) in
               if error != nil {
                   print(error!)
               }
               else{
                   do {
                       guard let content = data else {
                           print("no data")
                           return
                       }
                       
                       guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                           print("Bad json")
                           return
                       }
                       print(jsonArray)
                       
                       DispatchQueue.main.async {
                        if (jsonArray["codigoOperacion"] as! Int == 0){
                            if jsonArray["estatus"] as! Int == 2 {
                                guard let correo = self.tfUser.text else {return}
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                let context = appDelegate.persistentContainer.viewContext
                                let entity = NSEntityDescription.entity(forEntityName: "Usuario", in:
                                context)
                                let newProduct = NSManagedObject(entity: entity!, insertInto: context)
                                newProduct.setValue(jsonArray["idCliente"], forKey: "idCliente")
                                newProduct.setValue(jsonArray["token"], forKey: "token")
                                
                                do {
                                  try context.save()
                                } catch let error as NSError {
                                  print("No ha sido posible guardar \(error), \(error.userInfo)")
                                }

                                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
                                request.returnsObjectsAsFaults = false
                                
                                do {
                                    let result = try context.fetch(request)
                                    for data in
                                        result as! [NSManagedObject]{
                                            print(data.value(forKey: "token")!)
                                            print(data.value(forKey: "idCliente")!)
                                    }
                                }catch {
                                    print("Failed")
                                }
                                
                                let story = UIStoryboard(name: "Main", bundle: nil)
                                let control = story.instantiateViewController(withIdentifier: "Lista") as! ListaViewController
                                self.present(control, animated: true, completion: nil)
                            }
                            else if jsonArray["estatus"] as! Int == 1 {
                                let alert = UIAlertController(title: "TiendApp", message: "Antes de continuar debe tomar la foto necesaria de su identificación", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                    let story = UIStoryboard(name: "Main", bundle: nil)
                                    let control = story.instantiateViewController(withIdentifier: "Registro3") as! Registro3ViewController
                                    control.idCliente = (jsonArray["idCliente"] as! String)
                                    control.correo = correo
                                    self.present(control, animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else if jsonArray["estatus"] as! Int == 0 {
                                let alert = UIAlertController(title: "TiendApp", message: "Antes de continuar debe tomar la foto necesaria de su identificación", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                    let story = UIStoryboard(name: "Main", bundle: nil)
                                    let control = story.instantiateViewController(withIdentifier: "Registro2") as! Registro2ViewController
                                    control.idCliente = (jsonArray["idCliente"] as! String)
                                    control.correo = correo
                                    self.present(control, animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        else {
                            self.alert(mensaje: "Usuario y/o contraseña incorrecto")
                        }
                       }
                   }
               }
           }.resume()
       }
    
    func ObtenerLlave(){
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/obtenerclavepublica") else { return }
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
            let param:[String:String] = ["token" : "Hola"]
            guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
                print("fail")
                return
            }
                        
            print(String(data: jsonData, encoding: String.Encoding.utf8)!)
            
            request.httpBody = jsonData
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if error != nil {
                    print(error!)
                }
                else{
                    do {
                        guard let content = data else {
                            print("no data")
                            return
                        }
                        
                        guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                            print("Bad json")
                            return
                        }
                        print(jsonArray)
                        
                        if jsonArray["codigoOperacion"] as! Int == 0 {
                        }
                        else{
                            self.alert(mensaje: "Error")
                        }
                    }
                }
            }.resume()
    }
    
    func hideKeyboard()
       {
           let tap: UITapGestureRecognizer = UITapGestureRecognizer(
               target: self,
               action: #selector(self.dismissKeyboard))
           
           tap.cancelsTouchesInView = false
           view.addGestureRecognizer(tap)
       }
       
       @objc func dismissKeyboard()
       {
           view.endEditing(true)
       }

    func alert(mensaje:String){
           let alert = UIAlertController(title: "TiendApp", message: mensaje, preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
           self.present(alert, animated: true)
           
       }
}
