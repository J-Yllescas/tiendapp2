//
//  ResumenViewController.swift
//  iOSApp
//
//  Created by Javier Yllescas on 1/10/20.
//  Copyright © 2020 Javier Yllescas. All rights reserved.
//

import UIKit
import  CoreData

class ResumenViewController: UIViewController {

    
    @IBOutlet weak var lbCalle: UILabel!
    @IBOutlet weak var lbColonia: UILabel!
    @IBOutlet weak var lbCiudad: UILabel!
    @IBOutlet weak var lbNombreTarjeta: UILabel!
    @IBOutlet weak var lbNumeroTarjeta: UILabel!
    @IBOutlet weak var lbTotalProductos: UILabel!
    @IBOutlet weak var lbTotalCompra: UILabel!
    
    
    var direccioin:[String:String]?
    var Lista:[Dictionary<String, Any>] = []
    var total:String?
    var tarjeta:[String:String]?
    
    var usuarios:NSManagedObject?
    var cantidad:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        for l in Lista {
            let aux = (l["cantidad"] as! Int)
            cantidad = self.cantidad + aux
        }
        
        lbCalle.text = "Calle: \(String(describing: direccioin!["calle"]!))"
        lbColonia.text = "Colonia: \(String(describing: direccioin!["colonia"]!))"
        lbCiudad.text = "Ciudad: \(String(describing: direccioin!["ciudad"]!))"
        lbNombreTarjeta.text = "Nombre: \(String(describing: tarjeta!["nombre"]!))"
        lbNumeroTarjeta.text = "Número de tarjeta: \(String(describing: tarjeta!["numero"]!))"
        lbTotalProductos.text = "Número de productos: \(cantidad)"
        lbTotalCompra.text = total
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request2 = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        request2.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request2)
            for data in
                result as! [NSManagedObject]{
                    self.usuarios = data
            }
         try context.save()
        }catch {
            print("Failed")
        }
    }
    
    @IBAction func Comprar(_ sender: Any) {
        guard let totall = self.total else{return}
        comprar(idCliente: usuarios?.value(forKey: "idCliente") as! String, token: usuarios?.value(forKey: "token") as! String, total: totall, pedido: Lista)
    }
    
    func comprar(idCliente:String, token:String, total:String, pedido:[Dictionary<String, Any>]) {
        
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/realizarcompra") else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let param:[String:Any] = ["idCliente":idCliente, "token":token, "total":total, "pedido":pedido]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            print("fail")
            return
        }
                    
        print(String(data: jsonData, encoding: String.Encoding.utf8)!)
        
        request.httpBody = jsonData
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print(error!)
            }
            else{
                do {
                    guard let content = data else {
                        print("no data")
                        return
                    }
                    
                    guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                        print("Bad json")
                        return
                    }
                    print(jsonArray)
                    if jsonArray["codigoOperacion"] as? Int == 0 {
                        DispatchQueue.main.async {
                            self.eliminarCarrito()
                            let story = UIStoryboard(name: "Main", bundle: nil)
                            let control = story.instantiateViewController(identifier: "Final") as! FinalViewController
                            control.folio = jsonArray["folio"] as? String
                            self.present(control, animated: true, completion: nil)
                        }
                    }
                    else {
                        self.alert(mensaje: "La compra no pudo ser realizada")
                    }
                }
            }
        }.resume()
    }

    func alert(mensaje:String){
        let alert = UIAlertController(title: "TiendApp", message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func eliminarCarrito(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Producto")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for r in result as! [NSManagedObject] {
                context.delete(r)
            }
            try context.save()
        } catch let error as NSError {
            print(error.userInfo)
        }
    }
}
