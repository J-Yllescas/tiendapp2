//
//  CarritoViewController.swift
//  iOSApp
//
//  Created by Javier Yllescas on 1/3/20.
//  Copyright © 2020 Javier Yllescas. All rights reserved.
//

import UIKit
import CoreData

class CarritoViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tabla: UITableView!
    @IBOutlet weak var lbTotal: UILabel!
    
    var productos:[NSManagedObject] = []
    var Lista:[Dictionary<String, Any>] = []
    var usuarios:NSManagedObject?
    var precioTotal:Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabla.delegate = self
        tabla.dataSource = self
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Producto")
        request.returnsObjectsAsFaults = false
        
        let request2 = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        request2.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in
                result as! [NSManagedObject]{
                    self.productos.append(data)
            }
        }catch {
            print("Failed")
        }
        
        do {
            let result = try context.fetch(request2)
            for data in
                result as! [NSManagedObject]{
                    self.usuarios = data
            }
         try context.save()
        }catch {
            print("Failed")
        }
        
        total()
    }
    
    func total() {
        precioTotal = 0
        for a in productos{
            let aux:Double = a.value(forKey: "total") as! Double
            precioTotal = precioTotal + aux
        }
        lbTotal.text = precioTotal.redondear(numeroDeDecimales: 2)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(productos.count)
        return productos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cel = tableView.dequeueReusableCell(withIdentifier: "CarritoCell") as! CarritoTableViewCell
        cel.lbNombre.text = (self.productos[indexPath.row].value(forKey: "nombre") as! String)
        cel.lbPrecio.text = "Precio: $\(String(describing: self.productos[indexPath.row].value(forKey: "precio")!)) x "
        cel.lbStep.text = "\(String(describing: self.productos[indexPath.row].value(forKey: "cantidad")!))"
        let auxTotal:Double = self.productos[indexPath.row].value(forKey: "total") as! Double
        cel.lbTotal.text = "Total: $\(auxTotal.redondear(numeroDeDecimales: 2))"
        cel.stepper.value = productos[indexPath.row].value(forKey: "cantidad") as! Double
        cel.stepper.maximumValue = productos[indexPath.row].value(forKey: "stock") as! Double
        
        let url1:String = "\(self.productos[indexPath.row].value(forKey: "img")!)"
        let newURL = url1.replacingOccurrences(of: "\"", with: "")
        print(newURL)
        let url = URL(string: "\(newURL).jpg")
        print(url!)
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            if error == nil {
                DispatchQueue.main.async {
                    let image = UIImage(data: data!)
                    cel.img01.image = image
                }
            }
        }
        task.resume()
        
        self.Lista.append(["idProducto":self.productos[indexPath.row].value(forKey: "idProducto")!, "cantidad":self.productos[indexPath.row].value(forKey: "cantidad")!])
        
        cel.delegate = self
        cel.index = indexPath.row
        
        return cel
    }
    
    @IBAction func Comprar(_ sender: Any) {
        
        if precioTotal != 0 {
            let story = UIStoryboard(name: "Main", bundle: nil)
            let control = story.instantiateViewController(identifier: "Direccion") as! DireccionViewController
            guard let totall = lbTotal.text else{return}
            control.total = totall
            control.Lista = Lista
            present(control, animated: true, completion: nil)
        }
        else {
            alert(mensaje: "No tienes productos")
        }
        
    }
    
    var  menuDesplegabele = false
    let sizeWidth: CGFloat = UIScreen.main.bounds.size.width
    @IBOutlet weak var LeadingConstraint: NSLayoutConstraint!{
           didSet{
               LeadingConstraint.constant = sizeWidth
           }
       }
    
    @IBAction func Cerrar(_ sender: Any) {
      if (menuDesplegabele) {
          LeadingConstraint.constant = sizeWidth
       
      } else {
          let size: CGFloat = sizeWidth / 3
          LeadingConstraint.constant = (size * 2)
          UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
              self.view.layoutIfNeeded()
          })
      }
      menuDesplegabele = !menuDesplegabele
    }
    
    func cerrarSesion(token: String) {
          
          guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/logout") else { return }
     
          var request = URLRequest(url: url)
          request.httpMethod = "POST"
          request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
    
          let param:[String:String] = ["token": token]
      
          guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
              print("fail")
              return
          }
                      
          print(String(data: jsonData, encoding: String.Encoding.utf8)!)
        
          request.httpBody = jsonData
          let session = URLSession.shared
          session.dataTask(with: request) { (data, response, error) in
              if error != nil {
                  print(error!)
              }
              else{
                  do {
                      guard let content = data else {
                          print("no data")
                          return
                      }
                      
                      guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                          print("Bad json")
                          return
                      }
                      print(jsonArray)
                      
                      if jsonArray["codigoOperacion"] as! Int == 0 {
                          DispatchQueue.main.async {
                              let story = UIStoryboard(name: "Main", bundle: nil)
                              let control = story.instantiateViewController(withIdentifier: "Login") as! LoginViewController
                              self.present(control, animated: true, completion: nil)
                          }
                      } else {
                          self.alert(mensaje: "Error de cierre de Sesion")
                      }
                  }
                  
              }
          }.resume()
      }
    
    @IBAction func cerrarSesion(_ sender: Any) {
         
        cerrarSesion(token: usuarios?.value(forKey: "token") as! String)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
               let context = appDelegate.persistentContainer.viewContext
               
               let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
               request.returnsObjectsAsFaults = false
               
               do {
                   let result = try context.fetch(request)
                   for data in
                       result as! [NSManagedObject]{
                        context.delete(data)
                   }
                try context.save()
               }catch {
                   print("Failed")
               }
    }
    
    func alert(mensaje:String){
        let alert = UIAlertController(title: "TiendApp", message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
        
    }

}

extension CarritoViewController: Eliminar {
    func editar(index: Int, valor: Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        self.productos[index].setValue(valor, forKey: "cantidad")
        let aux2:Double = self.productos[index].value(forKey: "precio") as! Double * Double(valor)
        self.productos[index].setValue(aux2, forKey: "total")
        
        do {
            try context.save()
        }catch {
            print("Failed")
        }
        DispatchQueue.main.async {
            self.tabla.reloadData()
        }
        self.total()
    }
    
    func eliminar(index: Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
    
        do {
            context.delete(self.productos[index])
            self.productos.remove(at: index)
            try context.save()
        }catch {
            print("Failed")
        }
        
        print("Productos: \(self.productos.count)")
        DispatchQueue.main.async {
            self.tabla.reloadData()
        }
        
        self.total()
        
        alert(mensaje: "Producto eliminado con exito")
    }
    
    func saveProduct(nombre:String, descripcion:String, id:String, imagensrc:String, precio:Double, sku:Int, stock:Int) -> Product{
        let product:Product = Product(dictionary: ["nombre":nombre,"descripcion":descripcion, "id":id, "imagensrc":imagensrc, "precio":precio, "sku":sku, "stock":stock])
        return product
    }
    
    
}
