//
//  RegistroViewController.swift
//  iOSApp
//
//  Created by Javier Yllescas on 12/26/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit
import CoreData

class RegistroViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var tfNombre: UITextField!
    @IBOutlet weak var continuar2: UIButton!
    @IBOutlet weak var tfAPaterno: UITextField!
    @IBOutlet weak var tfAMaterno: UITextField!
    @IBOutlet weak var tfCorreo: UITextField!
    @IBOutlet weak var tfContraseña: UITextField!
    @IBOutlet weak var tfContraseña2: UITextField!
    
    @IBOutlet weak var viewNombre: UIView!
    @IBOutlet weak var viewAPaterno: UIView!
    @IBOutlet weak var viewAMaterno: UIView!
    @IBOutlet weak var viewCorreo: UIView!
    @IBOutlet weak var viewContraseña: UIView!
    @IBOutlet weak var viewContraseña2: UIView!
    
    var correo:String?
    
    var usuario:[String:Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        
        //Observadores
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification: )), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification: )), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification: )), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
    }

    @IBAction func Continuar(_ sender: Any) {
    
        guard let nombre = tfNombre.text else {return}
        guard let aPaterno = tfAPaterno.text else {return}
        guard let aMaterno = tfAMaterno.text else {return}
        guard let correo = tfCorreo.text else {return}
        guard let contraseña = tfContraseña.text else {return}
        guard let contraseña2 = tfContraseña2.text else {return}
        
        if nombre.espacionBlanco(cadena: nombre) == "" {
            alert(mensaje: "Por favor llene el campo 'Nombre'")
            viewNombre.backgroundColor = UIColor.red
        }
        else {
            if nombre.alfanumerico(cadena: nombre.lowercased()) == false {
                alert(mensaje: "Revise que el campo 'Nombre' sólo contenga caracteres alfanuméricos")
                viewNombre.backgroundColor = UIColor.red
            }
            else {
                viewNombre.backgroundColor = UIColor.black
                if aPaterno.espacionBlanco(cadena: aPaterno) == "" {
                    alert(mensaje: "Por favor llene el campo 'Apellido paterno'")
                    viewAPaterno.backgroundColor = UIColor.red
                }
                else {
                    if aPaterno.alfanumerico(cadena: aPaterno.lowercased()) == false {
                        alert(mensaje: "Revise que el campo 'Apellido paterno' sólo contenga caracteres alfanuméricos")
                        viewAPaterno.backgroundColor = UIColor.red
                    }
                    else {
                        viewAPaterno.backgroundColor = UIColor.black
                        if aMaterno.alfanumerico(cadena: aMaterno.lowercased()) == false {
                            alert(mensaje: "Revise que el campo 'Apellido materno' sólo contenga caracteres alfanuméricos")
                            viewAMaterno.backgroundColor = UIColor.red
                        }
                        else {
                            viewAMaterno.backgroundColor = UIColor.black
                            if correo.espacionBlanco(cadena: correo) == "" {
                                alert(mensaje: "Por favor llene el campo 'Correo'")
                                viewCorreo.backgroundColor = UIColor.red
                            }
                            else {
                                if correo.correo(cadena: correo) == false {
                                    alert(mensaje: "El correo electrónico no es válido")
                                    viewCorreo.backgroundColor = UIColor.red
                                }
                                else {
                                    viewCorreo.backgroundColor = UIColor.black
                                    if contraseña.espacionBlanco(cadena: contraseña) == "" {
                                        alert(mensaje: "Por favor llene el campo 'Contraseña'")
                                        viewContraseña.backgroundColor = UIColor.red
                                    }
                                    else {
                                        if contraseña.count < 8 {
                                            alert(mensaje: "El campo 'Contraseña' debe tener como mínimo 8 caracteres")
                                            viewContraseña.backgroundColor = UIColor.red
                                        }
                                        else {
                                            if contraseña != contraseña2 {
                                                alert(mensaje: "Las contraseñas no coinciden")
                                                viewContraseña.backgroundColor = UIColor.red
                                                viewContraseña2.backgroundColor = UIColor.red
                                            }
                                            else {
                                                viewContraseña.backgroundColor = UIColor.black
                                                viewContraseña2.backgroundColor = UIColor.black
                                                let user = saveUser(nombre: nombre, aPaterno: aPaterno, aMaterno: aMaterno, correo: correo, contraseña: contraseña)
                                                enviar(User: user, callback: {user in
                                                    self.usuario = user
                                                    DispatchQueue.main.async {
                                                        if self.usuario["codigoOperacion"] as! Int == 0{
                                                            let alert = UIAlertController(title: "TiendApp", message: "Datos registrados correctamente", preferredStyle: .alert)
                                                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                                                let story = UIStoryboard(name: "Main", bundle: nil)
                                                                let control = story.instantiateViewController(withIdentifier: "Registro2") as! Registro2ViewController
                                                                print("1.- \(self.usuario)")
                                                                control.idCliente = user["id"] as? String
                                                                self.correo = self.tfCorreo.text!
                                                                control.correo = self.correo
                                                                self.present(control, animated: true, completion: nil)
                                                                    print("Registrado exitosamente")
                                                            }))
                                                            self.present(alert, animated: true, completion: nil)
                                                        }
                                                        else{
                                                            self.alert(mensaje: "Correo ya registrado")
                                                        }
                                                    }
                                                })
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func saveUser(nombre:String, aPaterno:String, aMaterno:String, correo:String, contraseña:String) -> User{
        let user:User = User(dictionary: ["nombre":nombre, "aPaterno":aPaterno, "aMaterno":aMaterno, "correo":correo, "contrasena": contraseña])
        return user
    }
    
    
    func enviar(User:User, callback: @escaping ([String:Any]) -> ()) {
        
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/registrodeusuario") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let param:[String:String] = ["nombre" : User.nombre, "aPaterno" : User.aPaterno, "aMaterno" : User.aMaterno, "correo" : User.correo, "contrasena" : User.contraseña ]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            print("fail")
            return
        }
                    
        print(String(data: jsonData, encoding: String.Encoding.utf8)!)
        
        request.httpBody = jsonData
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                //print(response)
            }
            else{
                do {
                    guard let content = data else {
                        print("no data")
                        return
                    }
                    
                    guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                        print("Bad json")
                        return
                    }
                    print(jsonArray)
                    callback(jsonArray)
                }
            }
        }.resume()
    }
    
    func alert(mensaje:String){
        let alert = UIAlertController(title: "TiendApp", message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
        
    }
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    deinit {
            //Stop listening for keyboard hide/show events
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func keyboardWillChange(notification: Notification){
        
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        view.frame.origin.y = -keyboardRect.height + 250
        //view.safeAreaLayoutGuide.accessibilityFrame.origin.y = -keyboardRect.height + 250
        
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
        }else{
            view.frame.origin.y = 0
        }
    }
}
