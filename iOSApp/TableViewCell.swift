//
//  TableViewCell.swift
//  iOSApp
//
//  Created by Javier Yllescas on 12/30/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit
import CoreData

protocol Add {
    func agregar(producto:Product)
}

class TableViewCell: UITableViewCell {
    
    var delegate:Add?
    var producto:Product?

    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var lbNombre: UILabel!
    @IBOutlet weak var lbPrecio: UILabel!
    @IBOutlet weak var lbDisponible: UILabel!
    @IBOutlet weak var btnAgregar: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func Add(_ sender: Any) {
        print("add")
        
        guard let aux = producto else {
            return
        }
        
        delegate?.agregar(producto: aux)
    }
    
}
