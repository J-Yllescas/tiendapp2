//
//  ViewController.swift
//  iOSApp
//
//  Created by Javier Yllescas on 12/19/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit
import CoreData

class Registro2ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    let vc = UIImagePickerController()
    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var img01: UIImageView!
    @IBOutlet weak var btnC: UIButton!
    
    let posicion:String = "anverso"
    var usuario:[String:Any] = [:]
    var idCliente:String?
    var correo:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnC.isEnabled = false
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
      
        DispatchQueue.main.async {
            self.img01.image = image
        }
        btnC.isEnabled = true
   }
    
    @IBAction func camera01(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            vc.sourceType = .camera
            vc.allowsEditing = true
            vc.delegate = self
            present(vc, animated: true)
        }
        else{
            alert(mensaje: "Camara no disponible")
        }
    }
    
    @IBAction func continuar(_ sender: Any) {
        let seg = segmento()
        guard let imagen = img01.image else {return}
        let img = imagen.rezize(imagen: imagen)
        let base64Text = ConvertImageToBase64String(img: img)
        
        guard let idcliente = idCliente else{return}
        guard let correo = correo else{return}
        
            enviar(img: base64Text, posicion: posicion, tipo: seg, idCliente:idcliente, callback: { user in
                self.usuario = user
                if self.usuario["codigoOperacion"] as! Int == 0 {
                    DispatchQueue.main.async {
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let control = story.instantiateViewController(withIdentifier: "Registro3") as! Registro3ViewController
                        print("2.- \(self.usuario)")
                        print("2.- \(seg)")
                        control.tipo = seg
                        control.idCliente = idcliente
                        control.correo = correo
                        self.present(control, animated: true, completion: nil)
                    }
                }
                else{
                    self.alert(mensaje: "Error")
                }
            })
        
    }
    
    func ConvertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }
    
    func ConvertBase64StringToImage (imageBase64String:String) -> UIImage {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image!
    }
    
    func segmento() -> String {
        switch segment.selectedSegmentIndex {
        case 0:
            return "ine"
        case 1:
            return "ife"
        default:
            return ""
        }
    }
    
    func enviar(img:String, posicion:String, tipo:String, idCliente:String, callback: @escaping ([String:Any]) -> ()) {
        
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/registrodeidentificacion") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let param:[String:String] = ["imagen" : img, "posicion" : posicion, "tipo" : tipo, "idCliente":idCliente]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            print("fail")
            return
        }
                    
        print(String(data: jsonData, encoding: String.Encoding.utf8)!)
        
        request.httpBody = jsonData
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print(error!)
            }
            else{
                do {
                    guard let content = data else {
                        print("no data")
                        return
                    }
                    
                    guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                        print("Bad json")
                        return
                    }
                    print(jsonArray)
                    callback(jsonArray)
                }
            }
        }.resume()
    }
    
    func alert(mensaje:String){
        let alert = UIAlertController(title: "TiendApp", message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
        
    }
}

