//
//  HistorialTableViewCell.swift
//  iOSApp
//
//  Created by Javier Yllescas on 1/14/20.
//  Copyright © 2020 Javier Yllescas. All rights reserved.
//

import UIKit

class HistorialTableViewCell: UITableViewCell {

    @IBOutlet weak var lbFolio: UILabel!
    @IBOutlet weak var lbFecha: UILabel!
    @IBOutlet weak var lbTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
