//
//  ListaViewController.swift
//  iOSApp
//
//  Created by Javier Yllescas on 12/27/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit
import CoreData

class ListaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var Lista: UITableView!
    
    //var user:[String:Any]?
    var usuarios:NSManagedObject?
    var List:[Dictionary<String, Any>]?
    var productList:[Product] = []
    var productNS:[NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Lista.delegate = self
        Lista.dataSource = self
        
        print("nose")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in
                result as! [NSManagedObject]{
                    usuarios = data
            }
        }catch {
            print("Failed")
        }
        
        
        self.enviar(id: usuarios?.value(forKey: "idCliente") as! String, token: usuarios?.value(forKey: "token") as! String, callback: { json in
            self.List = json["productos"] as? [Dictionary<String, Any>]
            for l in self.List! {
                let p = self.saveProduct(nombre: l["nombre"]! as! String, descripcion: l["descripcion"]! as! String, id: l["id"]! as! String, imagensrc: l["imagensrc"]! as! String, precio: l["precio"]! as? Double ?? 0, sku: l["sku"]! as! Int, stock: l["stock"]! as! Int)
                self.productList.append(p)
            }
            print(self.productList.count)
            DispatchQueue.main.async {
                self.Lista.reloadData()
            }
        })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("el tamaño es de \(productList.count)")
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cel = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cel.lbNombre.text = productList[indexPath.row].nombre!
        cel.lbPrecio.text = "Precio: $\(String(describing: productList[indexPath.row].precio!))"
        cel.lbDisponible.text = "Disponible: \(String(describing: productList[indexPath.row].stock!))"
        
        let newURL = productList[indexPath.row].imagensrc!.replacingOccurrences(of: "\"", with: "")
        let url = URL(string: "\(newURL).jpg")
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            if error == nil {
                DispatchQueue.main.async {
                    let image = UIImage(data: data!)
                    cel.imgCell.image = image
                }
            }
        }
        task.resume()
        
        if productList[indexPath.row].stock! == 0 {
            cel.lbDisponible.textColor = UIColor.red
            cel.btnAgregar.tintColor = UIColor.red
        }
        else {
            cel.lbDisponible.textColor = UIColor.black
            cel.btnAgregar.tintColor = #colorLiteral(red: 0.991425693, green: 0.7912780643, blue: 0.2255264819, alpha: 1)
        }
        
        cel.delegate = self
        cel.producto = productList[indexPath.row]
        return cel
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let control = story.instantiateViewController(identifier: "Detalle") as DetalleViewController
        control.product = productList[indexPath.row]
        present(control, animated: true, completion: nil)
    }
    
    
    func saveProduct(nombre:String, descripcion:String, id:String, imagensrc:String, precio:Double, sku:Int, stock:Int) -> Product{
        let product:Product = Product(dictionary: ["nombre":nombre,"descripcion":descripcion, "id":id, "imagensrc":imagensrc, "precio":precio, "sku":sku, "stock":stock])
        return product
    }
    
    func enviar(id:String, token:String, callback: @escaping ([String:Any]) -> ()) {
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/catalogo") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let param:[String:String] = ["id" : id, "token" : token]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            print("fail")
            return
        }
                    
        print(String(data: jsonData, encoding: String.Encoding.utf8)!)
        
        request.httpBody = jsonData
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print(error!)
            }
            else{
                do {
                    guard let content = data else {
                        print("no data")
                        return
                    }
                    
                    guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                        print("Bad json")
                        return
                    }
                    print(jsonArray)
                    callback(jsonArray)
                }
            }
        }.resume()
    }
    
    var  menuDesplegabele = false
    let sizeWidth: CGFloat = UIScreen.main.bounds.size.width
    @IBOutlet weak var LeadingConstraint: NSLayoutConstraint!{
        didSet{
            LeadingConstraint.constant = sizeWidth
        }
    }
    
    @IBAction func Cerrar(_ sender: Any) {
      if (menuDesplegabele) {
          LeadingConstraint.constant = sizeWidth
       
      } else {
          let size: CGFloat = sizeWidth / 3
          LeadingConstraint.constant = (size * 2)
          UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
              self.view.layoutIfNeeded()
          })
      }
      menuDesplegabele = !menuDesplegabele
    }
    
    
    func cerrarSesion(token: String) {
          
          guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/logout") else { return }
     
          var request = URLRequest(url: url)
          request.httpMethod = "POST"
          request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
    
          let param:[String:String] = ["token": token]
      
          guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
              print("fail")
              return
          }
                      
          print(String(data: jsonData, encoding: String.Encoding.utf8)!)
          
          request.httpBody = jsonData
          
          let session = URLSession.shared
          
          session.dataTask(with: request) { (data, response, error) in
              if error != nil {
                  print(error!)
              }
              else{
                  do {
                      guard let content = data else {
                          print("no data")
                          return
                      }
                      
                      guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                          print("Bad json")
                          return
                      }
                      print(jsonArray)
                      
                      if jsonArray["codigoOperacion"] as! Int == 0 {
                          DispatchQueue.main.async {
                              let story = UIStoryboard(name: "Main", bundle: nil)
                              let control = story.instantiateViewController(withIdentifier: "Login") as! LoginViewController
                              self.present(control, animated: true, completion: nil)
                          }
                              
                          
                      } else {
                          self.alert(mensaje: "Error de cierre de Sesion")
                      }
                  }
                  
              }
          }.resume()
      }
      
    
    @IBAction func cerrarsesion(_ sender: Any) {
        
        cerrarSesion(token: usuarios?.value(forKey: "token") as! String)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in
                result as! [NSManagedObject]{
                    context.delete(data)
            }
            try context.save()
        }catch {
            print("Failed")
        }
    }
    
      func alert(mensaje:String){
          let alert = UIAlertController(title: "TiendApp", message: mensaje, preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
          self.present(alert, animated: true)
          
      }
    
    func agregarUno(idProducto:String, callback: @escaping ([String:Any]) -> ()){
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/verdisponibilidad") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let param:[String:String] = ["idProducto" : idProducto, "cantidad" : "1"]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            print("fail")
            return
        }
                       
        print(String(data: jsonData, encoding: String.Encoding.utf8)!)
           
        request.httpBody = jsonData
           
        let session = URLSession.shared
           
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print(error!)
            }
            else{
                do {
                    guard let content = data else {
                        print("no data")
                        return
                    }
                       
                    guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                        print("Bad json")
                        return
                    }
                    
                    print(jsonArray)
                    callback(jsonArray)
                }
            }
        }.resume()
    }
}

extension ListaViewController:Add {
    
    func agregar(producto: Product) {
        self.productNS = []
        
        agregarUno(idProducto: (producto.id)!, callback: { json in
            if json["codigoOperacion"] as! Int == 0 {
                DispatchQueue.main.async {
                    var existe:Bool = false
                    print(self.productNS.count)
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let context = appDelegate.persistentContainer.viewContext
                    
                    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Producto")
                    request.returnsObjectsAsFaults = false
                    
                    do {
                        let result = try context.fetch(request)
                        for data in
                            result as! [NSManagedObject]{
                                self.productNS.append(data)
                        }
                    }catch {
                        print("Failed")
                    }
                    
                        for product in self.productNS {
                            if product.value(forKey: "idProducto") as? String == producto.id {
                                let aux:Int16 = (product.value(forKey: "cantidad") as? Int16)!
                                if aux + 1 > (product.value(forKey: "stock") as? Int16)! {
                                    self.alert(mensaje: "No se puede agregar mas productos")
                                    existe = true
                                }
                                else {
                                    product.setValue(aux + 1, forKey: "cantidad")
                                    let aux2:Double = product.value(forKey: "total") as! Double
                                    product.setValue(aux2 + producto.precio!, forKey: "total")
                                    existe = true
                                    do{
                                        try context.save()
                                    }catch let error as NSError{
                                        print(error.userInfo)
                                    }
                                    self.alert(mensaje: "Producto agregado con exito")
                                }
                            }
                        }
                    
                    if existe == false {
                        let entity = NSEntityDescription.entity(forEntityName: "Producto", in:
                        context)
                        let newProduct = NSManagedObject(entity: entity!, insertInto: context)
                        newProduct.setValue(1, forKey: "cantidad")
                        newProduct.setValue((producto.nombre)!, forKey: "nombre")
                        newProduct.setValue(producto.precio!, forKey: "precio")
                        newProduct.setValue(producto.id!, forKey: "idProducto")
                        let total1:Double = (producto.precio!)
                        newProduct.setValue(total1, forKey: "total")
                        newProduct.setValue(producto.imagensrc!, forKey: "img")
                        newProduct.setValue(producto.stock, forKey: "stock")
                        self.alert(mensaje: "Producto agregado con exito")
                        
                        do{
                            try context.save()
                        }catch let error as NSError{
                            print(error.userInfo)
                        }
                    }
                }
            }
            else{
                DispatchQueue.main.async {
                    self.alert(mensaje: "Error al agregar producto")
                }
            }
        })
    }
}
