//
//  DireccionViewController.swift
//  iOSApp
//
//  Created by MAC on 08/01/20.
//  Copyright © 2020 Javier Yllescas. All rights reserved.
//

import UIKit

class DireccionViewController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var btnPicker: UIButton!
    @IBOutlet var tfCalle: UITextField!
    @IBOutlet var tfNumInt: UITextField!
    @IBOutlet var tfNumExt: UITextField!
    @IBOutlet var tfEstado: UITextField!
    @IBOutlet var tfmunicipio: UITextField!
    @IBOutlet var tfCodigoPostal: UITextField!
    @IBOutlet var tfColonia: UITextField!
    
    @IBOutlet var viewCalle: UIView!
    @IBOutlet var viewNumInt: UIView!
    @IBOutlet var viewNumExt: UIView!
    @IBOutlet var viewEstado: UIView!
    @IBOutlet var viewMunicipio: UIView!
    @IBOutlet var viewCodigoPostal: UIView!
    @IBOutlet var viewColonia: UIView!
    
    var Lista:[Dictionary<String, Any>] = []
    var total:String?
    
    var ciudades:[String] = []
    var auxpicker: String = ""
    var cp = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        
        tfEstado.isEnabled = false
        tfmunicipio.isEnabled = false
        tfCodigoPostal.delegate = self

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 5 {
            enviar(postal: tfCodigoPostal.text!, callback:{json in
                if json["error"] as? Int == 1 {
                    self.cp = false
                    DispatchQueue.main.async {
                        self.alert(mensaje: "Código postal no encontrado")
                        self.viewCodigoPostal.backgroundColor = UIColor.red
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self.cp = true
                        self.viewCodigoPostal.backgroundColor = UIColor.red
                        let response:[String:Any] = json["response"] as! [String : Any]
                        self.ciudades = response["asentamiento"] as! [String]
                        self.tfEstado.text = (response["estado"] as! String)
                        self.tfmunicipio.text = (response["municipio"] as! String)
                        self.piker()
                    }
                }
            })
        }
    }
    
    func alert(mensaje:String){
               let alert = UIAlertController(title: "TiendApp", message: mensaje, preferredStyle: .alert)
               alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
               self.present(alert, animated: true)
    }
    
    @IBAction func abrirPicker(_ sender: Any) {
        
    }
    
    func piker(){
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        self.tfColonia.inputView = pickerView
        let screenWidth = UIScreen.main.bounds.width
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(title: "Seleccionar", style: .plain, target: self, action: #selector(selectPressed))
        toolBar.setItems([flexibleSpace, doneBarButton], animated: false)
        self.tfColonia.inputAccessoryView = toolBar
        self.tfColonia.text = self.ciudades[0]
        self.auxpicker = self.ciudades[0]
    }
    @objc func selectPressed() {
        self.view.endEditing(true)
        
    }
    
    @IBAction func btnContinuar(_ sender: Any) {
        guard let calle = tfCalle.text else { return}
        guard let numInt = tfNumInt.text else {return}
        guard let numext = tfNumExt.text else {return}
        guard let estado = tfEstado.text else { return}
        guard let ciudad = tfmunicipio.text else {return}
        guard let postal = tfCodigoPostal.text else {return}
        guard let colonia = tfColonia.text else {return}
        
        if postal.espacionBlanco(cadena: postal) == "" {
            alert(mensaje: "Por favor llene el campo Codigo Postal")
            viewCodigoPostal.backgroundColor = UIColor.red
        }else{
            if postal.numerico(cadena: postal.lowercased()) == false{
                alert(mensaje: "Revise que el Codigo Postal sea numerico")
                viewCodigoPostal.backgroundColor = UIColor.red
            }else{
                if postal.count < 5 {
                    alert(mensaje: "El codigo postal no puede ser menos de 5 digitos")
                }else{
                    viewCodigoPostal.backgroundColor = UIColor.black
                    if calle.espacionBlanco(cadena: calle) == ""{
                        alert(mensaje: "Por favor llene el el campo Calle")
                        viewCalle.backgroundColor = UIColor.red
                    }else{
                        if calle.alfanumerico(cadena: calle.lowercased()) == false{
                            alert(mensaje: "Revise que el campo sea alfanumerico")
                            viewCalle.backgroundColor = UIColor.red
                        }else{
                           viewCalle.backgroundColor = UIColor.black
                            if numInt.numerico(cadena: numInt.lowercased()) == false{
                                alert(mensaje: "Revise que el campo sea numerico")
                                viewNumInt.backgroundColor = UIColor.red
                            }else{
                                viewNumInt.backgroundColor = UIColor.black
                                if numext.espacionBlanco(cadena: numext) == ""{
                                    alert(mensaje: "Por favor llene el campo Numero Exterior")
                                    viewNumExt.backgroundColor = UIColor.red
                                }else{
                                    if numext.numerico(cadena: numext.lowercased()) == false{
                                        alert(mensaje: "Revise que el campo Numero exterior sea numerico")
                                        viewNumExt.backgroundColor = UIColor.red
                                    }else{
                                        viewNumExt.backgroundColor = UIColor.black
                                        let direccioin:[String:String] = ["calle":calle, "colonia":colonia, "ciudad":ciudad]
                                        if cp == true {
                                            self.viewCodigoPostal.backgroundColor = UIColor.black
                                            guardarDireccion(calle: calle, colonia: colonia, exterior: numext, interior: numInt, cp: colonia, municipio: ciudad, ciudad: estado, callback: {
                                                json in
                                                if json["codigoOperacion"] as? Int == 0 {
                                                    DispatchQueue.main.async {
                                                        let alert = UIAlertController(title: "TiendApp", message: "Dirección registrada", preferredStyle: .alert)
                                                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                                            let story = UIStoryboard(name: "Main", bundle: nil)
                                                            let control = story.instantiateViewController(identifier: "Tarjeta") as! TarjetaViewController
                                                            control.Lista = self.Lista
                                                            control.total = self.total
                                                            control.direccioin = direccioin
                                                            self.present(control, animated: true, completion: nil)
                                                        }))
                                                        self.present(alert, animated: true, completion: nil)
                                                    }
                                                }
                                                else {
                                                    self.alert(mensaje: "Error al agregar Dirección")
                                                }
                                            })
                                        }
                                        else {
                                            self.viewCodigoPostal.backgroundColor = UIColor.red
                                            self.alert(mensaje: "El código postal no es válido")
                                        }
                                    }

                                }
                            }
                            
                        }
                    }
                }
         }
    }
 }
    
    
    func enviar(postal: String, callback: @escaping ([String:Any]) -> ()) {
        
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/consultarcp") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let param:[String:String] = ["codigoPostal" : postal ]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            print("fail")
            return
        }
                    
        print(String(data: jsonData, encoding: String.Encoding.utf8)!)
        
        request.httpBody = jsonData
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                //print(response)
            }
            else{
                do {
                    guard let content = data else {
                        print("no data")
                        return
                    }
                    
                    guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                        print("Bad json")
                        return
                    }
                    print(jsonArray)
                   callback(jsonArray)
                }
            }
      }.resume()
    }
    
    func guardarDireccion(calle: String,colonia:String, exterior:String, interior:String, cp:String, municipio:String, ciudad:String, callback: @escaping ([String:Any]) -> ()) {
        
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/registrardireccion") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let param:[String:String] = ["calle":calle, "colonia":colonia, "exterior":exterior, "interior":interior, "cp":cp, "municipio":municipio, "ciudad":ciudad ]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            print("fail")
            return
        }
                    
        print(String(data: jsonData, encoding: String.Encoding.utf8)!)
        
        request.httpBody = jsonData
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                //print(response)
            }
            else{
                do {
                    guard let content = data else {
                        print("no data")
                        return
                    }
                    
                    guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                        print("Bad json")
                        return
                    }
                    print(jsonArray)
                    callback(jsonArray)
                }
            }
      }.resume()
    }
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}
    
    extension DireccionViewController: UIPickerViewDelegate, UIPickerViewDataSource{
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return self.ciudades.count
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return self.ciudades[row]
        }
       
    
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            self.tfColonia.text = self.ciudades[row]
            self.auxpicker = self.ciudades[row]
        }
    
    }
    

  

