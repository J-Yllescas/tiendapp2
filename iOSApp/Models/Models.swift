//
//  Models.swift
//  iOSApp
//
//  Created by Javier Yllescas on 12/20/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

struct User: Decodable {
    let nombre:String
    let correo:String
    let aPaterno:String
    let aMaterno:String
    let contraseña:String
    
   init(dictionary: [String:Any]) {
       self.nombre = dictionary["nombre"] as? String ?? ""
       self.correo = dictionary["correo"] as? String ?? ""
       self.aPaterno = dictionary["aPaterno"] as? String ?? ""
       self.aMaterno = dictionary["aMaterno"] as? String ?? ""
       self.contraseña = dictionary["contrasena"] as? String ?? ""
   }
    
    func request() -> [String:Any]{
           return [
            "nombre":self.nombre,
            "correo":self.correo,
            "aPaterno":self.aPaterno,
            "aMaterno":self.aMaterno,
            "contrasena":self.contraseña
           ]
    }
}

struct Product {
    let imagensrc:String?
    let sku:Int?
    let descripcion:String?
    let nombre:String?
    let stock:Int?
    let precio:Double?
    let id:String?
    
    init(dictionary: [String:Any]) {
        self.nombre = dictionary["nombre"] as? String ?? ""
        self.imagensrc = dictionary["imagensrc"] as? String ?? ""
        self.sku = dictionary["sku"] as? Int ?? 0
        self.descripcion = dictionary["descripcion"] as? String ?? ""
        self.stock = dictionary["stock"] as? Int ?? 0
        self.precio = dictionary["precio"] as? Double ?? 0
        self.id = dictionary["id"] as? String ?? ""
    }
}

class RSA {
    
    var err: Unmanaged<CFError>?
    
    func generarLlave(){
        let tag = "com.example.keys.mykey".data(using: .utf8)!
        let attributes: [String: Any] =
            [kSecAttrKeyType as String:            kSecAttrKeyTypeRSA,
             kSecAttrKeySizeInBits as String:      2048,
             kSecPrivateKeyAttrs as String:
                [kSecAttrIsPermanent as String:    true,
                 kSecAttrApplicationTag as String: tag]
        ]

        guard let privateKey = SecKeyCreateRandomKey(attributes as CFDictionary, &err) else {
            print( err!.takeRetainedValue() as Error)
            return
        }
        
        let publicKey = SecKeyCopyPublicKey(privateKey)
        
        var b64Key:String?
        var b64Key2:String?
        
        if let cfdata = SecKeyCopyExternalRepresentation(publicKey!, &err) {
           let data:Data = cfdata as Data
           b64Key = data.base64EncodedString()
        }
        
        if let cfdata2 = SecKeyCopyExternalRepresentation(privateKey, &err) {
           let data:Data = cfdata2 as Data
           b64Key2 = data.base64EncodedString()
        }
        
        //print("privatekey: \(String(describing: b64Key2!))")
        //print("publickey: \(String(describing: b64Key!))")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Llave", in:
        context)
        
        let newProduct = NSManagedObject(entity: entity!, insertInto: context)
        newProduct.setValue(b64Key!, forKey: "privateKey")
        newProduct.setValue(b64Key2!, forKey: "publicKey")
        
        do {
          try context.save()
        } catch let error as NSError {
          print("No ha sido posible guardar \(error), \(error.userInfo)")
        }
    }
    
    func base64ToKey(b64Key:String) -> SecKey{
        let data2 = Data.init(base64Encoded: b64Key)

        let keyDict:[NSObject:NSObject] = [
           kSecAttrKeyType: kSecAttrKeyTypeRSA,
           kSecAttrKeyClass: kSecAttrKeyClassPublic,
           kSecAttrKeySizeInBits: NSNumber(value: 512),
           kSecReturnPersistentRef: true as NSObject
        ]
        
        let publicKey = SecKeyCreateWithData(data2! as CFData, keyDict as CFDictionary, nil)
    
        return publicKey!
    }
    
    func encriptar(publicKey:SecKey, plainText:String) -> String {
        let algorithm: SecKeyAlgorithm = .rsaEncryptionOAEPSHA512
        
        SecKeyIsAlgorithmSupported(publicKey, .encrypt, algorithm)
        
        if (plainText.count < (SecKeyGetBlockSize(publicKey)-130)){}else {
            print("eror")
        }
        
        let cfdata:CFData = plainText.data(using: .utf8)! as CFData
        print("cfdata: \(cfdata)")
        
        let cipherText = SecKeyCreateEncryptedData(publicKey,
                                                         algorithm,
                                                         cfdata,
                                                         &err) as Data?
        
        return (cipherText?.base64EncodedString())!
    }
    
    func desEncriptar(privateKey:SecKey, cipherText:String) -> String {
        let algorithm: SecKeyAlgorithm = .rsaEncryptionOAEPSHA512
        
        SecKeyIsAlgorithmSupported(privateKey, .decrypt, algorithm)
        
        if cipherText.count == SecKeyGetBlockSize(privateKey){}else {
            print("eror")
        }
        
        let cipherText2:CFData = cipherText as! CFData
        
        let clearText = SecKeyCreateDecryptedData(privateKey,
                                                        algorithm,
                                                        cipherText2,
                                                        &err) as Data?
        
        let d = String(decoding: clearText!, as: UTF8.self)
        return d
    }
    
    /*
     //set
     let key = privateKey
     let addquery: [String: Any] = [kSecClass as String: kSecClass,
                                    kSecAttrApplicationTag as String: tag,
                                    kSecValueRef as String: key]
     
     print(addquery)
     
     let status = SecItemAdd(addquery as CFDictionary, nil)
     print(status)
     guard status == errSecSuccess else {
         print("error")
         return
     }
     
     //get
     let getquery: [String: Any] = [kSecClass as String: kSecClassKey,
     kSecAttrApplicationTag as String: tag,
     kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
     kSecReturnRef as String: true]
     
     var item: CFTypeRef?
     let status2 = SecItemCopyMatching(getquery as CFDictionary, &item)
     guard status2 == errSecSuccess else {
     print("error2")
         return
     }
     let key2 = item as! SecKey
    */
}
