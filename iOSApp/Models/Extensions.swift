//
//  Extensions.swift
//  iOSApp
//
//  Created by Javier Yllescas on 12/26/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import Foundation
import UIKit
import Security

extension String
{
    func correo(cadena : String) -> Bool {
        print(cadena)
        var arreglo: [String] = []
        
        if let regex = try? NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}", options: .caseInsensitive)
           {
               let string = self as NSString

               arreglo = regex.matches(in: self, options: [], range: NSRange(location: 0, length: string.length)).map {
                   string.substring(with: $0.range).replacingOccurrences(of: "#", with: "").lowercased()
               }
           }
        let string = arreglo.joined(separator: "")
        return cadena == string
    }
    
    func alfanumerico(cadena : String) -> Bool {
        let letters = cadena.map { String($0) }
        var arreglo: [String] = []
        
        if let regex = try? NSRegularExpression(pattern: "[a-z0-9áéíóúñ ]", options: .caseInsensitive)
           {
               let string = self as NSString

               arreglo = regex.matches(in: self, options: [], range: NSRange(location: 0, length: string.length)).map {
                   string.substring(with: $0.range).replacingOccurrences(of: "#", with: "").lowercased()
               }
           }
        return letters == arreglo
    }
    
    func numerico(cadena : String) -> Bool {
        let letters = cadena.map { String($0) }
        var arreglo: [String] = []
        
        if let regex = try? NSRegularExpression(pattern: "[1234567890]", options: .caseInsensitive)
           {
               let string = self as NSString

               arreglo = regex.matches(in: self, options: [], range: NSRange(location: 0, length: string.length)).map {
                   string.substring(with: $0.range).replacingOccurrences(of: "#", with: "").lowercased()
               }
           }
        return letters == arreglo
    }
    
    func espacionBlanco(cadena: String) -> String {
        return cadena.replacingOccurrences(of: " ", with: "")
    }
}

extension Double {
    func redondear(numeroDeDecimales: Int) -> String {
        let formateador = NumberFormatter()
        formateador.maximumFractionDigits = numeroDeDecimales
        formateador.roundingMode = .down
        return formateador.string(from: NSNumber(value: self)) ?? ""
    }
}

private var maxLengths = [UITextField: Int]()

extension UITextField {

   @IBInspectable var maxLength: Int {

      get {

          guard let length = maxLengths[self]
             else {
                return Int.max
      }
      return length
   }
   set {
      maxLengths[self] = newValue
      addTarget(
         self,
         action: #selector(limitLength),
         for: UIControl.Event.editingChanged
      )
   }
}
    @objc func limitLength(textField: UITextField) {
    guard let prospectiveText = textField.text,
        prospectiveText.count > maxLength
    else {
        return
    }

   let selection = selectedTextRange
   let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
   text = prospectiveText.substring(to: maxCharIndex)
   selectedTextRange = selection
   }
}


extension UIImage {
    func rezize(imagen: UIImage) -> UIImage {
        let ancho: CGFloat = imagen.size.width
        let alto: CGFloat = imagen.size.height
        let imgRelacion = ancho / alto
        let maxWidth:CGFloat = 640
        let newHeight:CGFloat = maxWidth / imgRelacion
        let constanteCompresion:CGFloat = 0.5
        let rect:CGRect = CGRect(x: 0, y: 0, width: maxWidth, height: newHeight)
        UIGraphicsBeginImageContext(rect.size)
        imagen.draw(in: rect)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData:Data = img.jpegData(compressionQuality: constanteCompresion)!
        UIGraphicsEndImageContext()
        return UIImage(data: imageData)!
    }
}
