//
//  Enums.swift
//  iOSApp
//
//  Created by Javier Yllescas on 12/24/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import Foundation

enum idType {
    case INE
    case IFE
}

enum url:String {
    case url = "http://74.208.239.104:8080/"
    case url2 = "http://tiendapp.rood-estudio.com/"
}
