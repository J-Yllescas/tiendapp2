//
//  HistorialViewController.swift
//  iOSApp
//
//  Created by Javier Yllescas on 1/13/20.
//  Copyright © 2020 Javier Yllescas. All rights reserved.
//

import UIKit
import CoreData

class HistorialViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var listaHistorial: UITableView!
    
    var user:NSManagedObject?
    var pedido:[Dictionary<String, Any>] = []
    var lista:[Dictionary<String, Any>] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listaHistorial.delegate = self
        listaHistorial.dataSource = self
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in
                result as! [NSManagedObject]{
                    user = data
            }
        }catch {
            print("Failed")
        }
        
        consultarHistorial(token: user?.value(forKey: "token") as! String, callback: {
            json in
            if json["codigoOperacion"] as? Int == 0 {
                self.pedido = json["pedido"] as! [Dictionary<String, Any>]
                for p in self.pedido {
                    self.lista.append(p)
                }
                DispatchQueue.main.async {
                    self.listaHistorial.reloadData()
                }
            }
            else {
                print("Error")
            }
        })
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(lista.count)
        return lista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HistorialTableViewCell
        cell.lbFolio.text! = "Folio: \(lista[indexPath.row]["folio"] as! String)"
        let date = Date(timeIntervalSince1970: lista[indexPath.row]["fecha"]! as! Double / 1000.0)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "GMT-06:00")
        dateFormatter.locale = NSLocale(localeIdentifier: "es-MX") as Locale
        dateFormatter.dateFormat = "eeee-dd-MMM-yyyy HH:mm"
        let strDate = dateFormatter.string(from: date)
        cell.lbFecha.text! = "Fecha: \(strDate)"
        cell.lbTotal.text! = "Total: $\(String(describing: (lista[indexPath.row]["total"]!)))"
        return cell
    }
    
    func consultarHistorial(token:String, callback: @escaping ([String:Any]) -> ()) {
        guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/consultahistorialpedido") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let param:[String:String] = ["token":token]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
            print("fail")
            return
        }
        
        print(String(data: jsonData, encoding: String.Encoding.utf8)!)
        request.httpBody = jsonData
        let sesion = URLSession.shared
        sesion.dataTask(with: request){ (data, response, error) in
            if error != nil {
                print(error!)
            }
            else {
                do {
                    guard let content = data else {
                        print("no data")
                        return
                    }
                    
                    guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String:Any]) else {
                        print("Bad json")
                        return
                    }
                    print(jsonArray)
                    callback(jsonArray)
                }
            }
        }.resume()
    }
    
    var  menuDesplegabele = false
    let sizeWidth: CGFloat = UIScreen.main.bounds.size.width
    @IBOutlet weak var LeadingConstraint: NSLayoutConstraint!{
        didSet{
            LeadingConstraint.constant = sizeWidth
        }
    }
    
    @IBAction func Cerrar(_ sender: Any) {
      if (menuDesplegabele) {
          LeadingConstraint.constant = sizeWidth
       
      } else {
          let size: CGFloat = sizeWidth / 3
          LeadingConstraint.constant = (size * 2)
          UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
              self.view.layoutIfNeeded()
          })
      }
      menuDesplegabele = !menuDesplegabele
    }
    
    func cerrarSesion(token: String) {
          
          guard let url = URL(string: "\(url.url2.rawValue)servicios/api/bdm/tiendapp/logout") else { return }
     
          var request = URLRequest(url: url)
          request.httpMethod = "POST"
          request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
    
          let param:[String:String] = ["token": token]
      
          guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: []) else {
              print("fail")
              return
          }
                      
          print(String(data: jsonData, encoding: String.Encoding.utf8)!)
          request.httpBody = jsonData
          let session = URLSession.shared
          
          session.dataTask(with: request) { (data, response, error) in
              if error != nil {
                  print(error!)
              }
              else{
                  do {
                      guard let content = data else {
                          print("no data")
                          return
                      }
                      
                      guard let jsonArray = (try? JSONSerialization.jsonObject(with: content, options: .mutableContainers) as? [String: Any]) else {
                          print("Bad json")
                          return
                      }
                      print(jsonArray)
                      
                      if jsonArray["codigoOperacion"] as! Int == 0 {
                          DispatchQueue.main.async {
                              let story = UIStoryboard(name: "Main", bundle: nil)
                              let control = story.instantiateViewController(withIdentifier: "Login") as! LoginViewController
                              self.present(control, animated: true, completion: nil)
                          }
                          
                      } else {
                          self.alert(mensaje: "Error de cierre de Sesion")
                      }
                  }
                  
              }
          }.resume()
      }
    
    @IBAction func cerrarSesion(_ sender: Any) {
         
        cerrarSesion(token: user?.value(forKey: "token") as! String)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
               let context = appDelegate.persistentContainer.viewContext
               
               let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuario")
               request.returnsObjectsAsFaults = false
               
               do {
                   let result = try context.fetch(request)
                   for data in
                       result as! [NSManagedObject]{
                        context.delete(data)
                   }
                try context.save()
               }catch {
                   print("Failed")
               }
    }
    
    func alert(mensaje:String){
        let alert = UIAlertController(title: "TiendApp", message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
        
    }
}
